from mbee import serialstar
import serial
import json
import time
import requests
from threading import Thread
import numpy

PORT = "/dev/ttyAMA0"
BITRATE = 9600
storage_values_params = {'h':{}, 'd':{}, 'i':{}, 'c':{}}
basic_values_params = {} #{'id_in_system1':{data_type:'', 'source_address'}, 'id_in_system2':{'data_type':'', }}
mbee = serialstar.SerialStar(PORT, BITRATE)


def work_with_config():
    with open('config.json', 'r') as f:
        file_config = json.load(f)
    return file_config

file_config = work_with_config()

#Получение токена
def getToken(login, password):
    data = {'login':login,'password':password}
    return requests.post('https://dev-test.1sim.online/api2/auth/open', json=data).json()['token']

#Формирование запросов
def work_with_api(link, type_request, data=None):
    try:
        token = getToken(login=file_config['auth']['login'], password=file_config['auth']['password'])
        headers = {'Authorization':'Bearer ' + token}
        return requests.request(type_request, 'https://dev-test.1sim.online/api2/'+link, headers=headers, json=data).json()
    except OSError as e:
        print(e)
        time.sleep(60)
        return work_with_api(link, type_request, data)

#Добавление параметров устройства на платформу
def addParams():
    input_value_for_api = {}
    input_value_for_api['values'] = []
    for i in range(len(file_config['devices'])):
        input_value_for_api['id'] = file_config['devices'][i]['id'] #id_device
        for j in range(len(file_config['devices'][i]['params'])):
            input_value_for_api['values'].append({'node_num':file_config['devices'][i]['params'][j]['values']['node_num'],
                                                  'code': file_config['devices'][i]['params'][j]['values']['code'],
                                                  'physicname': file_config['devices'][i]['params'][j]['values']['physicname'],
                                                  'data_type': file_config['devices'][i]['params'][j]['values']['data_type'],
                                                  'descr': file_config['devices'][i]['params'][j]['values']['descr'],
                                                  'aggr_mode': file_config['devices'][i]['params'][j]['values']['aggr_mode'],
                                                  'measurement':file_config['devices'][i]['params'][j]['values']['measurement']})
    response = work_with_api(data=input_value_for_api, link='device/values', type_request='PUT')
    for i in range(len(file_config['devices'])):
        for j in range(len(file_config['devices'][i]['params'])):
            file_config['devices'][i]['params'][j]['values']['id'] = response['status'][j]['id']
    print(file_config)

#Добавление значений в параметры
def addValuesParams(value_id, val):
    input_value_for_api = {}
    input_value_for_api['dt'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    input_value_for_api['data'] = []
    input_value_for_api['data'].append({'value_id': value_id,   #id параметра
                                            'val': val,
                                            'fault': '0'})
    print('Добавление значений в  параметры')
    print(input_value_for_api)
    print()
    print(work_with_api(data=input_value_for_api, link='values/data', type_request='PUT'))

#Связь параметра и адреса, прибора, из пакета
def parsingConfig():
    global basic_values_params, storage_values_params
    for i in range(len(file_config['devices'])):
        for j in range(len(file_config['devices'][i]['params'])):
            frame_output = file_config['devices'][i]['params'][j]['values']['frame_output']
            if not(frame_output in basic_values_params):
                basic_values_params[frame_output] ={}

            basic_values_params[frame_output][file_config['devices'][i]['params'][j]['values']['id']] = {}
            basic_values_params[frame_output][file_config['devices'][i]['params'][j]['values']['id']]['source_address'] = file_config['devices'][i]['params'][j]['values']['source_address_dec']
            # Подразумевается, что если не указан pin, то параметр связан с характеристикой модуля (VCC, RSSI, TEMPERATURE),
            # название которой должно быть записано в конфиге, в поле - 'name_value'
            if 'pin' in file_config['devices'][i]['params'][j]['values']:
                if file_config['devices'][i]['params'][j]['values']['pin'] != '':
                    basic_values_params[frame_output][file_config['devices'][i]['params'][j]['values']['id']]['pin'] = file_config['devices'][i]['params'][j]['values']['pin']
                else:
                    basic_values_params[frame_output][frame_output][file_config['devices'][i]['params'][j]['values']['id']]['name_value'] = file_config['devices'][i]['params'][j]['values']['name_value']
            elif not ('pin' in file_config['devices'][i]['params'][j]['values']== ''):
                basic_values_params[frame_output][file_config['devices'][i]['params'][j]['values']['id']]['name_value'] = file_config['devices'][i]['params'][j]['values']['name_value']
            basic_values_params[frame_output][file_config['devices'][i]['params'][j]['values']['id']]['data_type'] =  file_config['devices'][i]['params'][j]['values']['data_type']

            if "min_val" in file_config['devices'][i]['params'][j]['values']:
                 basic_values_params[frame_output][file_config['devices'][i]['params'][j]['values']['id']]['min_val'] = file_config['devices'][i]['params'][j]['values']['min_val']
            if "max_val" in file_config['devices'][i]['params'][j]['values']:
                 basic_values_params[frame_output][file_config['devices'][i]['params'][j]['values']['id']]['max_val'] = file_config['devices'][i]['params'][j]['values']['max_val']
            if "mathexpstr" in file_config['devices'][i]['params'][j]['values']:
                 basic_values_params[frame_output][file_config['devices'][i]['params'][j]['values']['id']]['mathexpstr'] = file_config['devices'][i]['params'][j]['values']['mathexpstr']
            if "hours_update" in file_config['devices'][i]['params'][j]['values']:
                 basic_values_params[frame_output][file_config['devices'][i]['params'][j]['values']['id']]['hours_update'] = file_config['devices'][i]['params'][j]['values']['hours_update']


            for k in basic_values_params[frame_output]:
                if basic_values_params[frame_output][k]['data_type'] == 'h':
                    storage_values_params['h'][k] = []
                elif basic_values_params[frame_output][k]['data_type'] == 'd':
                    storage_values_params['d'][k] = []
                elif basic_values_params[frame_output][k]['data_type'] == 'i':
                    storage_values_params['i'][k] = []
                elif basic_values_params[frame_output][k]['data_type'] == 'c':
                    storage_values_params['c'][k] = []

    print('====================================================================================================')
    print('УПРАЩЕННЫЙ СЛОВАРЬ - basic_values_params')
    print(basic_values_params)
    print()
    print(storage_values_params)


def decode_value_frame(data, id_param):
    if 'ANALOG_INPUT' in data:
        if 'mathexpstr' in basic_values_params['83'][id_param]:
            data_packet = int(data['ANALOG_INPUT'],16)
            return eval(basic_values_params['83'][id_param]['mathexpstr'])
        elif ('max_val' in basic_values_params['83'][id_param]) and ('min_val' in basic_values_params['83'][id_param]):
            M1 = numpy.array([[0, 1], [4096, 1]])
            v1 = numpy.array([basic_values_params['83'][id_param]['min_val'], basic_values_params['83'][id_param]['max_val']])
            k, b = numpy.linalg.solve(M1, v1)
            return int(data['ANALOG_INPUT'], 16) * k + b
        else: print('NIKUDA NA ZASHOL')
    elif 'COUNTER_INPUT_1' in data:
        return int(data['COUNTER_INPUT_1'], 16)
    elif 'COUNTER_INPUT_2' in data:
         return int(data['COUNTER_INPUT_2'], 16)

def data_type_h():
    global storage_values_params
    while True:
        time.sleep(60)
        h = storage_values_params
        print('storage_values_params[h] ДО ВЗАИМОДЕЙСВТИЯ ' + str(h['h']))
        summa = 0
        for i in h['h']:
            summa = 0
            if len(h['h'][i]) != 0:
                for j in h['h'][i]:
                    summa += j
                addValuesParams(value_id=i, val=summa/len(h['h'][i]))
            storage_values_params['h'][i] = []

#storage_values_params = {'h':{'12131':[val1, val2], '13123412':[val1, val2]}, 'd':{}, 'i':{}}
def data_type_c():
    global storage_values_params
    while True:
        time.sleep(3)
        c = storage_values_params
        print('storage_values_params[c] ДО ВЗАИМОДЕЙСВТИЯ ' + str(c['c']))
        for i in c['c']:
            summa = 0
            if len(c['c'][i]) != 0:
                for j in c['c'][i]:
                    summa += j
                addValuesParams(value_id=i, val=summa/len(c['c'][i]))
            storage_values_params['c'][i] = []


def data_type_d():
    global storage_values_params
    while True:
        time.sleep(120)
        d = storage_values_params
        print('storage_values_params[d] ДО ВЗАИМОДЕЙСВТИЯ ' + str(d['d']))
        for i in d['d']:
            summa = 0
            if len(d['d'][i]) != 0:
                for j in d['d'][i]:
                    summa += j
                addValuesParams(value_id=i, val=summa/len(d['d'][i]))
            storage_values_params['d'][i] = []

def data_type_i():
    global storage_values_params
    while True:
        summa = 0
        minutes = time.strftime('%M', time.localtime())
        seconds = time.strftime('%S', time.localtime())
        if (minutes == '00') and (seconds == '15'):
            minutes = time.strftime('%M', time.localtime())
            seconds = time.strftime('%S', time.localtime())
            for i in storage_values_params['i'].keys():
                if len(storage_values_params['i'][i]) != 0:
                    for j in storage_values_params['i'][i]:
                        summa += i
                    addValuesParams(value_id=i, val=summa)
                storage_values_params['i'][j] = []
            time.sleep(3595)

def add_values_in_category(type_frame,val, i):
        if basic_values_params[type_frame][i]['data_type'] == 'c':
              storage_values_params['c'][i].append(val)
        elif basic_values_params[type_frame][i]['data_type'] == 'h':
              storage_values_params['h'][i].append(val)
        elif basic_values_params[type_frame][i]['data_type'] == 'd':
              storage_values_params['d'][i].append(val)
        elif basic_values_params[type_frame][i]['data_type'] == 'i':
              storage_values_params['i'][i].append(val)


# Определение callback-функции для получения данных о текущем состоянии активных линий ввода/вывода на удаленном модеме.
def frame_83_received(packet):
    global storage_values_params, basic_values_params
    moment_value = {} #данные полученные с последнего frame
    SOURCE_ADDRESS = packet['SOURCE_ADDRESS_DEC']
    val = None
    for i in basic_values_params['83']:
        if SOURCE_ADDRESS == basic_values_params['83'][i]['source_address']:
            if ('pin' in basic_values_params['83'][i]):
                if not (basic_values_params['83'][i]['pin'] == ''):
                    try:
                        val = decode_value_frame(packet['DATA_PARSED_DECODED'][basic_values_params['83'][i]['pin']], i)
                    except KeyError:
                        print('Информации по указанному пину нет ' + str(i))
            else:
                try:
                    val = packet[basic_values_params['83'][i]['name_value']]
                except KeyError:
                    print('В пакете нет параметра, который был указан в \'name_value\' ')

            if not(val == None):
                add_values_in_category('83', val, i)
            else: print('Значение не удалось получить! '+ str(i))
#Отправка запросов на счетчик. Тип фрейма: 01
def electricity_meters_request_service():
    global fsm2
    while True:
        if fsm2 == 1:
            mbee.send_tx_request("00", "0009", "2F3F210D0A", "01")
            time.sleep(0.25)
            mbee.send_tx_request("00", "0009", "063035310D0A", "01")
            time.sleep(0.25)
            mbee.send_tx_request("00", "0009", "01523102455430504528290337", "01")
            fsm2 = 2
        elif fsm2 ==2:
            mbee.send_tx_request("00", "0009", "2F3F210D0A", "01")
            time.sleep(0.25)
            mbee.send_tx_request("00", "0009", "063035310D0A", "01")
            time.sleep(0.25)
            mbee.send_tx_request("00", "0009", "01523102504F57455028290364", "01")
            fsm2 = 1
        time.sleep(3)


def remote_uart_data_received(packet):
    val = None
    for i in basic_values_params['8F']:
        if packet["SOURCE_ADDRESS_DEC"] == basic_values_params['8F'][i]['source_address']:
            if basic_values_params['8F'][i]['name_value'] == 'reading':
                data = (packet["DATA"])
                s = (data[0:14])
                if s == "02455430504528":
                    val = float(bytes.fromhex(data[14:22]))
                    add_values_in_category('8F', val, i)
                # elif s == "02504F57455028":
                #     power = float(bytes.fromhex(data[14:22]))
            if basic_values_params['8F'][i]['name_value'] == 'RSSI':
                val = packet['RSSI']
                add_values_in_category('8F', val, i)
            #elif basic_values_params['8F'][i]['name_value'] == 'VCC':
            #    print(packet)
            #    val = packet['VCC']
            else: print('Значение не получено ' + str(i))

# #Регистрация callback-функций
mbee.callback_registring("83", frame_83_received)
mbee.callback_registring("81", remote_uart_data_received)
mbee.callback_registring("8F", remote_uart_data_received)

fsm2 = 1

def loop():
    global storage_values_params
    while True:
        try:
            mbee.run()
        except serial.serialutil.SerialException:
            print('Error')

thread1 = Thread(target=data_type_h)
thread2 = Thread(target=data_type_d)
thread3 = Thread(target=data_type_i)
thread4 = Thread(target=data_type_c)
thread5 = Thread(target=electricity_meters_request_service)

if __name__ == "__main__":
    addParams()
    parsingConfig()
    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()
    thread5.start()
    loop()

#================================Светодиодные ленты==============================
def onLight(destination_id):
    mbee.send_remote_at("00", "0008", "01", "M1", "0CB2")
    mbee.send_remote_at("00", "0008", "01", "M2", "32C8")
    mbee.send_remote_at("00", "0008", "05", "M3", "1964")
 #Отправка с помощью AT-команд(стр. 4 и 14 (СериалСтар))
    # "32C8" - 13000, максимальное напряжение на ШИМ1 (Такой параметр только для M1)

def offLight(destination_id):
    mbee.send_remote_at("00", "0008", "01", "M1", "0000")
    mbee.send_remote_at("00", "0008", "01", "M2", "0000")
    mbee.send_remote_at("00", "0008", "05", "M3", "0000")
